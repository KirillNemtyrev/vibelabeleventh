package org.example;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in, StandardCharsets.UTF_8);

        String s = scanner.nextLine();
        Map<String, Integer> map = Arrays.stream(s.split(" "))
                .map(String::toLowerCase)
                .collect(Collectors.toMap(v -> v, v -> 1, Integer::sum));

        List<String> collect = map.entrySet().stream()
                .sorted(Comparator.comparingInt(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .limit(10)
                .collect(Collectors.toList());

        System.out.println(collect);
    }
}